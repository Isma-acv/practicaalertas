package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.VM_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public String APITOKEN, NOMBREUSUARIO;

    ///// *** oncreate

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            Intent exiToken= new Intent(MainActivity.this,Inicio.class);
           startActivity(exiToken);
            //Toast.makeText(this,"Token guadado",Toast.LENGTH_LONG).show();
        }
    }

    //--

    public void PetcionLogeo(View view){

        EditText EdtUsuario  = (EditText) findViewById(R.id.editTextCorreo);
        EditText EdtPassword = (EditText) findViewById(R.id.editTextContrasena);
        String usuario = EdtUsuario.getText().toString();
        String password = EdtPassword.getText().toString();

        if(TextUtils.isEmpty(usuario)){
            EdtUsuario.setError("Escriba su Usuario");
            EdtUsuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(password)){
            EdtPassword.setError("Escriba su contraseña");
            EdtPassword.requestFocus();
            return;
        }

        //----- peticion

        ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
        Call<VM_Login> logeoCall = service.get_Login(EdtUsuario.getText().toString(), EdtPassword.getText().toString());
        logeoCall.enqueue(new Callback<VM_Login>() {
            @Override
            public void onResponse(Call<VM_Login> call, Response<VM_Login> response) {

                VM_Login peticion = response.body();

                if (response.body() == null){
                    showToastbad();
                    return;
                }
                if (peticion.estado == "true"){
                    APITOKEN = peticion.token;
                    guardartoken();
                    NOMBREUSUARIO = peticion.id;
                    guardarName();
                    showToast();
                    Intent Iniciar = new Intent(MainActivity.this,Inicio.class);
                    startActivity(Iniciar);


                }else {
                    Toast.makeText(MainActivity.this,peticion.detalle,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VM_Login> call, Throwable t) {
                showToastbad();
            }
        });


    }

    //--

    public void Registro(View view){
        Intent registro = new Intent(MainActivity.this,Registro.class);
        startActivity(registro);
    }

    //--

    public void showToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));
        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);
        toastText.setText("Bienvenido");
        toastImage.setImageResource(R.drawable.ic_toasticon);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //--

    public void showToastbad() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout_bad, (ViewGroup) findViewById(R.id.toast_rootbad));
        TextView toastText = layout.findViewById(R.id.toast_textbad);
        ImageView toastImage = layout.findViewById(R.id.toast_imagebad);
        toastText.setText("Algo salio mal");
        toastImage.setImageResource(R.drawable.ic_baseline_error_outline_24);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //--

    public void guardartoken(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }

    public void guardarName(){
        SharedPreferences preferenciasname = getSharedPreferences("credencialesname", Context.MODE_PRIVATE);
        String name = NOMBREUSUARIO;
        SharedPreferences.Editor editor = preferenciasname.edit();
        editor.putString("NAME", name);
        editor.commit();
    }

}