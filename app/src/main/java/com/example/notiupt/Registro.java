package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.VM_Regsitro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    public EditText nombreUsuario, password1, password2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


    }

    //--
    public void Peticion(View view){

        nombreUsuario = (EditText) findViewById(R.id.EdtEmail);
        password1 = (EditText) findViewById(R.id.editTextContrasena);
        password2 = (EditText) findViewById(R.id.EdtPasswordDetype);

        String nombreUs = nombreUsuario.getText().toString();
        String pass1 = password1.getText().toString();
        String pass2 = password2.getText().toString();

        // -- Verficicar que no esten  nulos los campos


        if(TextUtils.isEmpty(nombreUs)){
            nombreUsuario.setError("Agrege un nombre de Usuario");
            nombreUsuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(pass1)){
            password1.setError("Porfavor Ingrese una contraseña");
            password1.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(pass2)){
            password2.setError("Porfavor repita su contraseña");
            password2.requestFocus();
            return;
        }
        if(!pass1.equals(pass2)){
            password1.setError("LAS CONTRASEÑAS NO COINCIDEN");
            password1.requestFocus();
            password2.setError("LAS CONTRASEÑAS NO COINCIDEN");
            password2.requestFocus();
            return;
        }

        // -- peticion

        ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
        Call<VM_Regsitro> registrarCall = service.R_Usuario(nombreUsuario.getText().toString(), password1.getText().toString());
        registrarCall.enqueue(new Callback<VM_Regsitro>() {
            @Override
            public void onResponse(Call<VM_Regsitro> call, Response<VM_Regsitro> response) {

                VM_Regsitro peticion = response.body();
                if (response.body() == null){
                    showToastbad();
                    return;
                }
                if (peticion.estado == "true"){
                    startActivity(new Intent(Registro.this,MainActivity.class));
                    showToast();
                }else {
                    Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VM_Regsitro> call, Throwable t) {
                    showToastbad();
            }
        });

    }

    // --
    public void Inicio(View view){
        Intent inicio = new Intent(Registro.this,MainActivity.class);
        startActivity(inicio);
    }

    //--

    public void showToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));
        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);
        toastText.setText("Listo! Ya puedes iniciar");
        toastImage.setImageResource(R.drawable.ic_toasticon);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //--

    public void showToastbad() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout_bad, (ViewGroup) findViewById(R.id.toast_rootbad));
        TextView toastText = layout.findViewById(R.id.toast_textbad);
        ImageView toastImage = layout.findViewById(R.id.toast_imagebad);
        toastText.setText("Algo salio mal");
        toastImage.setImageResource(R.drawable.ic_baseline_error_outline_24);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}