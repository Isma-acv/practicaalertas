package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.Clases.Clase_Agenerales;
import com.example.notiupt.ViewModels.VM_Agenerales;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Agenerales extends AppCompatActivity {

    // AREGLOS QUE DONDE SE VAN A GUADAR LA RESPUESTA DEL JSON
    ArrayList<String> CatalogoUsuariosid = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();
    ArrayList<String> Catalogoup = new ArrayList<>();
    ArrayList<String> CatalogouCre = new ArrayList<>();

    //-- VARIABLES DE LOS ELEMENTOS QUE VOY A OCUPAR
    public ListView lvlistaAlertasG;
    public TextView tvid, tvuid, tvup, tvcre;
    //-- ARREGLOS QUE VAN A HEREDAR EL CONTENIDO DE LOS ARRAYLIST
    public String cid [];
    public String create [];
    public String update [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenerales);

        tvid = (TextView) findViewById(R.id.textViewid);
        tvuid = (TextView) findViewById(R.id.textViewusuarioid);
        tvup = (TextView) findViewById(R.id.textViewUpdate);
        tvcre = (TextView) findViewById(R.id.textViewCreate);
        lvlistaAlertasG = (ListView) findViewById(R.id.lvlistaAlertasGenerales);

        //--------------------------------------------- Codigo ListView
       final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,CatalogoId);
        lvlistaAlertasG.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvid.setText("ID: " +lvlistaAlertasG.getItemAtPosition(position));
                tvuid.setText("UsuarioId: "+ cid[position]);
                tvup.setText("updated_at: " + update[position]);
                tvcre.setText("create_at: " + create[position]);
            }
        });
        //----------------
        ServicioPeticion service = Api.getApi(Agenerales.this).create(ServicioPeticion.class);
        Call<VM_Agenerales> iniciarcall = service.get_Agenerales();
        iniciarcall.enqueue(new Callback<VM_Agenerales>() {
            @Override
            public void onResponse(Call<VM_Agenerales> call, Response<VM_Agenerales> response) {
                VM_Agenerales peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Agenerales.this,"ERROR 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                else{

                    List<Clase_Agenerales> detalle = peticion.alertas;

                    for (Clase_Agenerales mostrar : detalle){
                        CatalogoId.add(mostrar.getId());
                    }
                    lvlistaAlertasG.setAdapter(arrayAdapter);
                    //---------------------------------------------- Catalogo usuarioId
                    for (Clase_Agenerales mostrarusuarioid : detalle){
                        CatalogoUsuariosid.add(mostrarusuarioid.getUsuarioId());
                    }
                    cid = CatalogoUsuariosid.toArray(new String[CatalogoUsuariosid.size()]);
                    //---------------------------------------------- Catalogo update
                    for (Clase_Agenerales mostrarup : detalle){
                        Catalogoup.add(mostrarup.getUpdated_at());
                    }
                    update = Catalogoup.toArray(new String[Catalogoup.size()]);
                    //---------------------------------------------- Catalogo Create
                    for (Clase_Agenerales mostrarcre : detalle){
                        CatalogouCre.add(mostrarcre.getUpdated_at());
                    }
                    create = CatalogouCre.toArray(new String[CatalogouCre.size()]);

                }
            }// else

            @Override
            public void onFailure(Call<VM_Agenerales> call, Throwable t) {
                Toast.makeText(Agenerales.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });// cierre metodos peticion
        // -- peticion Alertas Generales


    }//oncreate

}//clase