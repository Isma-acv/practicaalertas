package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.Clases.Clase_Ausuario;
import com.example.notiupt.ViewModels.VM_Ausuario;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Ausuario extends AppCompatActivity {

    // AREGLOS QUE DONDE SE VAN A GUADAR LA RESPUESTA DEL JSON
    ArrayList<String> CatalogoUsuariosid = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();
    ArrayList<String> Catalogoup = new ArrayList<>();
    ArrayList<String> CatalogouCre = new ArrayList<>();

    //-- VARIABLES DE LOS ELEMENTOS QUE VOY A OCUPAR
    public ListView lvlistaAlertasU;
    public TextView tvid, tvuid, tvup, tvcre;
    //-- ARREGLOS QUE VAN A HEREDAR EL CONTENIDO DE LOS ARRAYLIST
    public String cid [];
    public String create [];
    public String update [];
    String usuarioId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ausuario);
        //-- obtengo el nombre
        TextView Name = (TextView) findViewById(R.id.textViewNma);
        usuarioId = getIntent().getStringExtra("name");
        Name.setText(usuarioId);
        // asigno valores a los elementos
        tvid = (TextView) findViewById(R.id.textViewid);
        tvuid = (TextView) findViewById(R.id.textViewuid);
        tvup = (TextView) findViewById(R.id.textViewup);
        tvcre = (TextView) findViewById(R.id.textViewcre);
        lvlistaAlertasU = (ListView) findViewById(R.id.listviewausuario);

        //--------------------------------------------- Codigo ListView
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,CatalogoId);
        lvlistaAlertasU.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvid.setText("ID: " +lvlistaAlertasU.getItemAtPosition(position));
                tvuid.setText("UsuarioId: "+ cid[position]);
                tvup.setText("updated_at: " + update[position]);
                tvcre.setText("create_at: " + create[position]);
            }
        });

        ServicioPeticion service = Api.getApi(Ausuario.this).create(ServicioPeticion.class);
        Call<VM_Ausuario> iniciarcall = service.get_Ausuario(usuarioId);
        iniciarcall.enqueue(new Callback<VM_Ausuario>() {
            @Override
            public void onResponse(Call<VM_Ausuario> call, Response<VM_Ausuario> response) {
                VM_Ausuario peticion = response.body();
                if (peticion.estado.equals("true")){

                    List<Clase_Ausuario> detalle = peticion.alertas;

                    for (Clase_Ausuario mostrar : detalle){
                        CatalogoId.add(mostrar.getId());
                    }
                    lvlistaAlertasU.setAdapter(arrayAdapter);
                    //---------------------------------------------- Catalogo usuarioId
                    for (Clase_Ausuario mostrarusuarioid : detalle){
                        CatalogoUsuariosid.add(mostrarusuarioid.getUsuarioId());
                    }
                    cid = CatalogoUsuariosid.toArray(new String[CatalogoUsuariosid.size()]);
                    //---------------------------------------------- Catalogo update
                    for (Clase_Ausuario mostrarup : detalle){
                        Catalogoup.add(mostrarup.getUpdated_at());
                    }
                    update = Catalogoup.toArray(new String[Catalogoup.size()]);
                    //---------------------------------------------- Catalogo Create
                    for (Clase_Ausuario mostrarcre : detalle){
                        CatalogouCre.add(mostrarcre.getUpdated_at());
                    }
                    create = CatalogouCre.toArray(new String[CatalogouCre.size()]);

                }else {
                    Toast.makeText(Ausuario.this,"ERROR 500",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VM_Ausuario> call, Throwable t) {
                Toast.makeText(Ausuario.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });



    }//oncreate
}//Clase