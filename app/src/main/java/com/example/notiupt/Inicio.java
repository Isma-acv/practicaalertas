package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.VM_Ausuario;
import com.example.notiupt.ViewModels.VM_Calerta;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Inicio extends AppCompatActivity {
    String nombreUsuario;
    TextView Nombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
         Nombre = (TextView) findViewById(R.id.textViewUsuario);
        SharedPreferences preferenciasname = getSharedPreferences("credencialesname", Context.MODE_PRIVATE);
        String name = preferenciasname.getString("NAME", "");
        if (name != ""){
                Nombre.setText(name);
        }
         nombreUsuario = Nombre.getText().toString();

    }//oncreate

    public void  CrearAlertaPeticion(View view){
        //-- petcion para crear alerta

        ServicioPeticion service = Api.getApi(Inicio.this).create(ServicioPeticion.class);
        Call<VM_Calerta> iniciarcall = service.get_Calerta(nombreUsuario);
        iniciarcall.enqueue(new Callback<VM_Calerta>() {
            @Override
            public void onResponse(Call<VM_Calerta> call, Response<VM_Calerta> response) {
                VM_Calerta peticion= response.body();
                if (peticion.estado.equals("true")){
                    showToast();
                }else {
                    showToastbad();
                }
            }

            @Override
            public void onFailure(Call<VM_Calerta> call, Throwable t) {
                Toast.makeText(Inicio.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });

    } //metodoCRear alerta

    //--- toast de alertas creadas

    public void showToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));
        TextView toastText = layout.findViewById(R.id.toast_text);
        ImageView toastImage = layout.findViewById(R.id.toast_image);
        toastText.setText("ALERTA CREADA");
        toastImage.setImageResource(R.drawable.ic_toasticon);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //-- toast de error

    public void showToastbad() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout_bad, (ViewGroup) findViewById(R.id.toast_rootbad));
        TextView toastText = layout.findViewById(R.id.toast_textbad);
        ImageView toastImage = layout.findViewById(R.id.toast_imagebad);
        toastText.setText("No se creo Alerta");
        toastImage.setImageResource(R.drawable.ic_baseline_error_outline_24);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //-- Button Alertas Generales
    public void goAgenerales(View view){
        Intent go = new Intent(Inicio.this,Agenerales.class);
        startActivity(go);
    }
    // --
    public void goAusuario(View view){
        Intent go = new Intent(Inicio.this,Ausuario.class);
        go.putExtra("name",nombreUsuario);
        startActivity(go);
    }
    // --
    public void goVusuario(View view){
        Intent go = new Intent(Inicio.this,Vusuario.class);
        go.putExtra("name",nombreUsuario);
        startActivity(go);
    }
    // --

    // --
    public void goVgenerales(View view){
        Intent go = new Intent(Inicio.this,Vgenerales.class);
        startActivity(go);
    }
    // --
}//clase