package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.Clases.Clase_Ausuario;
import com.example.notiupt.ViewModels.Clases.Clase_Vusuario;
import com.example.notiupt.ViewModels.VM_Ausuario;
import com.example.notiupt.ViewModels.VM_Vusuario;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vusuario extends AppCompatActivity {
    ArrayList<String> Catalogovisualizaciones = new ArrayList<>();
    String usuarioId;
    public ListView lvlistaVisualizacionesU;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vusuario);
        lvlistaVisualizacionesU = (ListView) findViewById(R.id.lvVisualizacionUsuario);
        TextView Name = (TextView) findViewById(R.id.textView5name);
        usuarioId = getIntent().getStringExtra("name");
        Name.setText(usuarioId);
        //--------------------------------------------------------
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,Catalogovisualizaciones);
        //----------------peticion

        ServicioPeticion service = Api.getApi(Vusuario.this).create(ServicioPeticion.class);
        Call<VM_Vusuario> iniciarcall = service.get_Vusuario(usuarioId);
        iniciarcall.enqueue(new Callback<VM_Vusuario>() {
            @Override
            public void onResponse(Call<VM_Vusuario> call, Response<VM_Vusuario> response) {
                VM_Vusuario peticion = response.body();
               if (peticion.estado.equals("true")){

                       List<Clase_Vusuario> detalle = peticion.visualizaciones;
                       for (Clase_Vusuario mostrar : detalle){
                           Catalogovisualizaciones.add("ID  "+mostrar.getId());
                           Catalogovisualizaciones.add("UsuarioID   "+mostrar.getUsuarioId());
                           Catalogovisualizaciones.add("AlertaId    "+mostrar.getAlertaId());
                           Catalogovisualizaciones.add("Created_at  "+mostrar.getCreated_at());
                           Catalogovisualizaciones.add("Updated_at  "+mostrar.getUpdated_at());
                       }
                       lvlistaVisualizacionesU.setAdapter(arrayAdapter);

            }else{
                Toast.makeText(Vusuario.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
                }
            }//Onresponse

            @Override
            public void onFailure(Call<VM_Vusuario> call, Throwable t) {
                Toast.makeText(Vusuario.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });



    }
}