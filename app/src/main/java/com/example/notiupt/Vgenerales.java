package com.example.notiupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notiupt.Api.Api;
import com.example.notiupt.Servicio.ServicioPeticion;
import com.example.notiupt.ViewModels.Clases.Clase_Agenerales;
import com.example.notiupt.ViewModels.Clases.Clase_Vgenerales;
import com.example.notiupt.ViewModels.VM_Agenerales;
import com.example.notiupt.ViewModels.VM_Vgenerales;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vgenerales extends AppCompatActivity {

    // AREGLOS QUE DONDE SE VAN A GUADAR LA RESPUESTA DEL JSON
    ArrayList<String> CatalogoUsuariosid = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();
    ArrayList<String> Catalogoup = new ArrayList<>();
    ArrayList<String> CatalogouCre = new ArrayList<>();
    ArrayList<String> CatalogoaId = new ArrayList<>();

    //-- VARIABLES DE LOS ELEMENTOS QUE VOY A OCUPAR
    public ListView lvlistaVisualizacionesG;
    public TextView tvid, tvuid, tvup, tvcre, tvaid;
    //-- ARREGLOS QUE VAN A HEREDAR EL CONTENIDO DE LOS ARRAYLIST
    public String cid [];
    public String create [];
    public String update [];
    public String aid [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vgenerales);

        tvid = (TextView) findViewById(R.id.tvgid);
        tvuid = (TextView) findViewById(R.id.tvguid);
        tvup = (TextView) findViewById(R.id.tvgup);
        tvcre = (TextView) findViewById(R.id.tvgcreated);
        tvaid = (TextView) findViewById(R.id.tvgalertaid);
        lvlistaVisualizacionesG = (ListView) findViewById(R.id.lvg);
//--------------------------------------------- Codigo ListView
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,CatalogoId);
        lvlistaVisualizacionesG.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvid.setText("ID: " +lvlistaVisualizacionesG.getItemAtPosition(position));
                tvuid.setText("UsuarioId: "+ cid[position]);
                tvup.setText("updated_at: " + update[position]);
                tvcre.setText("create_at: " + create[position]);
                tvaid.setText("alertaId: " + aid[position]);
            }
        });

        //*************

        ServicioPeticion service = Api.getApi(Vgenerales.this).create(ServicioPeticion.class);
        Call<VM_Vgenerales> iniciarcall = service.get_Vgenerales();
        iniciarcall.enqueue(new Callback<VM_Vgenerales>() {
            @Override
            public void onResponse(Call<VM_Vgenerales> call, Response<VM_Vgenerales> response) {
                VM_Vgenerales peticion = response.body();
                if (response.body() == null){
                    Toast.makeText(Vgenerales.this,"ERROR 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    List<Clase_Vgenerales> detalle = peticion.visualizaciones;

                    for (Clase_Vgenerales mostrar : detalle){
                        CatalogoId.add(mostrar.getId());
                    }
                    lvlistaVisualizacionesG.setAdapter(arrayAdapter);
                    //---------------------------------------------- Catalogoid
                    for (Clase_Vgenerales mostrarusuarioid : detalle){
                        CatalogoUsuariosid.add(mostrarusuarioid.getUsuarioId());
                    }
                    cid = CatalogoUsuariosid.toArray(new String[CatalogoUsuariosid.size()]);
                    //---------------------------------------------- Catalogo update
                    for (Clase_Vgenerales mostrarup : detalle){
                        Catalogoup.add(mostrarup.getUpdated_at());
                    }
                    update = Catalogoup.toArray(new String[Catalogoup.size()]);
                    //---------------------------------------------- Catalogo Create
                    for (Clase_Vgenerales mostrarcre : detalle){
                        CatalogouCre.add(mostrarcre.getUpdated_at());
                    }
                    create = CatalogouCre.toArray(new String[CatalogouCre.size()]);
                    //---------------------------------------------- Catalogo alertaid
                    for (Clase_Vgenerales mostraraid : detalle){
                        CatalogoaId.add(mostraraid.getAlertaId());
                    }
                    aid = CatalogoaId.toArray(new String[CatalogoaId.size()]);
                }
            }

            @Override
            public void onFailure(Call<VM_Vgenerales> call, Throwable t) {

            }
        });

    }//Oncreate
}