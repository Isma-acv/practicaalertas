package com.example.notiupt.ViewModels;

import com.example.notiupt.ViewModels.Clases.Clase_Agenerales;
import com.example.notiupt.ViewModels.Clases.Clase_Vgenerales;

import java.util.ArrayList;

public class VM_Vgenerales {
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<Clase_Vgenerales> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(ArrayList<Clase_Vgenerales> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public String estado;
    public String detalle;
    public ArrayList<Clase_Vgenerales> visualizaciones;


}
