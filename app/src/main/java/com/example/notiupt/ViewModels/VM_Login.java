package com.example.notiupt.ViewModels;

public class VM_Login {

    public String contrasena;
    public String token;
    public String id;
    public String detalle;
    public String estado;
    public String nameusuario;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public VM_Login(String nameusuario, String contrasena){
        this.nameusuario = nameusuario;
        this.contrasena = contrasena;
    }



    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNameusuario() {
        return nameusuario;
    }

    public void setNameusuario(String nameusuario) {
        this.nameusuario = nameusuario;
    }


}
