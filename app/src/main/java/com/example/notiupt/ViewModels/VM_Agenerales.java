package com.example.notiupt.ViewModels;

import com.example.notiupt.ViewModels.Clases.Clase_Agenerales;

import java.util.ArrayList;

public class VM_Agenerales {

    public String estado;
    public String detalle;
    public ArrayList <Clase_Agenerales> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<Clase_Agenerales> getAlertas() {
        return alertas;
    }

    public void setAlertas(ArrayList<Clase_Agenerales> alertas) {
        this.alertas = alertas;
    }

}
