package com.example.notiupt.ViewModels;

import com.example.notiupt.ViewModels.Clases.Clase_Ausuario;
import com.example.notiupt.ViewModels.Clases.Clase_Vusuario;

import java.util.ArrayList;
import java.util.List;

public class VM_Vusuario {
    public String estado;
    public String detalle;
    public String usuarioId;
    public List<Clase_Vusuario> visualizaciones;
    public VM_Vusuario(String usuarioId){
        this.usuarioId = usuarioId;
    }


    public List<Clase_Vusuario> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Clase_Vusuario> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }




}
