package com.example.notiupt.ViewModels;

import com.example.notiupt.Ausuario;
import com.example.notiupt.ViewModels.Clases.Clase_Agenerales;
import com.example.notiupt.ViewModels.Clases.Clase_Ausuario;

import java.util.ArrayList;

public class VM_Ausuario {
    public String estado;
    public String detalle;
    public String usuarioId;
    public ArrayList<Clase_Ausuario> alertas;

   public VM_Ausuario(String usuarioId){
        this.usuarioId = usuarioId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public ArrayList<Clase_Ausuario> getAlertas() {
        return alertas;
    }

    public void setAlertas(ArrayList<Clase_Ausuario> alertas) {
        this.alertas = alertas;
    }


}
