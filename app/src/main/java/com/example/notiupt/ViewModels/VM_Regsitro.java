package com.example.notiupt.ViewModels;

public class VM_Regsitro {
    public String correo;
    public String password;
    public String detalle;
    public String estado;
    // Constructor
    public VM_Regsitro(String correo, String password){
        this.correo = correo;
        this.password = password;
    }

    // Get & Set
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
