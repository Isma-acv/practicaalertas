package com.example.notiupt.Servicio;

import com.example.notiupt.ViewModels.VM_Agenerales;
import com.example.notiupt.ViewModels.VM_Ausuario;
import com.example.notiupt.ViewModels.VM_Calerta;
import com.example.notiupt.ViewModels.VM_Login;
import com.example.notiupt.ViewModels.VM_Regsitro;
import com.example.notiupt.ViewModels.VM_Vgenerales;
import com.example.notiupt.ViewModels.VM_Vusuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<VM_Regsitro> R_Usuario(@Field("username")String correo, @Field("password")String password);

   @FormUrlEncoded
    @POST("api/login")
    Call<VM_Login> get_Login(@Field("username")String nameusuario, @Field("password")String contrasena);

   //Alertas Generales
   @POST("api/alertas")
   Call<VM_Agenerales> get_Agenerales();

    //Alertas Usuario
    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<VM_Ausuario> get_Ausuario(@Field("usuarioId")String usuarioId);

    //Crear alertas
    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<VM_Calerta> get_Calerta(@Field("usuarioId")String usuarioId);

    //Ver visualizacion de usuario
    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<VM_Vusuario> get_Vusuario(@Field("usuarioId")String usuarioId);

    //Visualizaciones Generales
    @POST("api/visualizaciones")
    Call<VM_Vgenerales> get_Vgenerales();

}

